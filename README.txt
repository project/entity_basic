Basic Entity
============

Required Modules
================

- Entity API

Optional Modules
================

- Rules
- Views

Installation
============
* Copy the entity_basic directory to the modules folder in your installation.
* Go to the Modules page (/admin/modules) and enable it.
