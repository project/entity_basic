<?php

/**
 * @file
 * A class used for entity basic.
 */

/**
 * The class used for Entity Basic entities.
 */
class EntityBasic extends Entity {

  /**
   * The unique id this entity.
   *
   * @var string
   */
  public $id;

  /**
   * The basic entity type.
   *
   * @var string
   */
  public $type;

  /**
   * The basic entity creation timestamp.
   *
   * @var string
   */
  public $created;

  /**
   * The basic entity updated timestamp.
   *
   * @var string
   */
  public $updated;

  /**
   * The user id.
   *
   * @var string
   */
  public $uid;

  public function __construct($values = array()) {
    if (!isset($values['uid']) && isset($values['user'])) {
      $values['uid'] = $values['user']->uid;
      unset($values['user']);
    }
    if (isset($values['type']) && is_object($values['type'])) {
      $values['type'] = $values['type']->name;
    }

    parent::__construct($values, 'entity_basic');
    if (!isset($this->uid)) {
      $this->uid = $GLOBALS['user']->uid;
    }
    if (!isset($this->timestamp)) {
      $this->timestamp = time();
    }
  }

  /**
   * Returns the user associated with the entity_basic.
   */
  public function getUser() {
    return user_load($this->uid);
  }

  /**
   * Sets a new user associated with the entity_basic.
   *
   * @param $account
   *   The user account object or the user account id (uid).
   */
  public function setUser($account) {
    $this->uid = is_object($account) ? $account->uid : $account;
  }

  /**
   * Gets the associated entity_basic type.
   *
   * @return entity_basicType
   */
  public function getType() {
    return entity_basic_type_load($this->type);
  }

  /**
   * Override this in order to implement a custom default URI and specify
   * 'entity_class_uri' as 'uri callback' hook_entity_info().
   */
  protected function defaultUri() {
    return array('path' => 'entity_basic/' . $this->identifier());
  }

}

/**
 * The Controller for the Basic Entity (CRUD).
 */
class EntityBasicAPIController extends EntityAPIController {

  /**
   * Implements EntityAPIControllerInterface.
   */
  public function create(array $values = array()) {
    global $user;
    return parent::create($values);
  }

  /**
   * Implements EntityAPIControllerInterface.
   *
   * @param $content
   *   Optionally. Allows pre-populating the built content to ease overridding
   *   this method.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $build = parent::buildContent($entity, $view_mode, $langcode, $content);

    $build['view_mode'] = array(
        '#type' => 'markup',
        '#markup' => $view_mode,
    );

    return $build;
  }

}

/**
 * UI controller
 */
class EntityBasicUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults. Main reason for doing this is that
   * parent class hook_menu() is optimized for entity type administration.
   */
  public function hook_menu() {

    $items = array();
    $id_count = count(explode('/', $this->path));
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%' . $this->entityType;

    $items[$this->path] = array(
      'title' => 'Basics',
      'description' => 'Add edit and update basics.',
      'page callback' => 'entity_basic_admin_content',
      'access arguments' => array('administer entity basic'),
      'file' => 'includes/entity_basic.entity_basic.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );

    // Change the overview menu type for the list of models.
    $items[$this->path]['type'] = MENU_LOCAL_TASK;

    // Change the add page menu to multiple types of entities
    $items[$this->path . '/add'] = array(
      'title' => 'Add a basic',
      'description' => 'Add a new basic',
      'page callback' => 'drupal_goto',
      'page arguments' => array('entity_basic/add'),
      'access callback'  => 'entity_basic_access',
      'access arguments' => array('edit'),
      'type' => MENU_LOCAL_ACTION,
      'weight' => 20,
      'file' => 'includes/entity_basic.entity_basic.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );

    $items['entity_basic'] = array(
      'title' => 'Basics',
      'description' => 'Add edit and update basics.',
      'page callback' => 'entity_basic_admin_content',
      'access arguments' => array('administer entity basic'),
      'file' => 'includes/entity_basic.entity_basic.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module']),
      'menu_name' => 'navigation',
      'type' => MENU_CALLBACK,
    );
    // IMPORTANT TO HAVE MENU_NORMAL_ITEM.
    $items['entity_basic/add'] = array(
      'title' => 'Add a basic',
      'description' => 'Add a new basic',
      'page callback'  => 'entity_basic_add_page',
      'access callback'  => 'entity_basic_access',
      'access arguments' => array('edit'),
      'type' => MENU_NORMAL_ITEM,
      'weight' => 20,
      'file' => 'includes/entity_basic.entity_basic.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );

    // Add menu items to add each different type of entity.
    foreach (entity_basic_type_load() as $type) {
      $items['entity_basic/add/' . $type->type] = array(
        'title' => 'Add ' . $type->name,
        'page callback' => 'model_form_wrapper',
        'page arguments' => array(entity_basic_create(array('type' => $type->type))),
        'access callback' => 'model_access',
        'access arguments' => array('edit', 'edit ' . $type->type),
        'file' => 'model.admin.inc',
        'file path' => drupal_get_path('module', $this->entityInfo['module'])
      );
    }

    // Loading and editing entity_basic entities
    $items[$this->path . '/entity_basic/' . $wildcard] = array(
      'page callback' => 'model_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'model_access',
      'access arguments' => array('edit', $id_count + 1),
      'weight' => 0,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'model.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );
    $items[$this->path . '/entity_basic/' . $wildcard . '/edit'] = array(
      'title' => 'Edit',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    );

    $items[$this->path . '/entity_basic/' . $wildcard . '/delete'] = array(
      'title' => 'Delete',
      'page callback' => 'model_delete_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'model_access',
      'access arguments' => array('edit', $id_count + 1),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_INLINE,
      'weight' => 10,
      'file' => 'model.admin.inc',
       'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );

    // Menu item for viewing models
    $items['entity_basic/' . $wildcard] = array(
      //'title' => 'Title',
      'title callback' => 'model_page_title',
      'title arguments' => array(1),
      'page callback' => 'model_page_view',
      'page arguments' => array(1),
      'access callback' => 'model_access',
      'access arguments' => array('view', 1),
      'type' => MENU_CALLBACK,
    );
    return $items;
  }

  /**
   * Page callback: Displays add content links for available content types.
   *
   * Redirects to node/add/[type] if only one content type is available.
   *
   * @see hook_menu()
   */
  public function addPage() {
    $item = menu_get_item();
    $content = system_admin_menu_block($item);
    if (count($content) == 1) {
      $item = array_shift($content);
      drupal_goto($item['href']);
    }
    return theme('basic_entity_add_list', array('content' => $content));
  }

  /**
   * Overridden to customize the field location.
   */
  public function entityFormSubmitBuildEntity($form, &$form_state) {

    // We cannot use entity_form_submit_build_entity() any more.
    // We have extra fields which needs to be saved.
    $entity = $form_state['bmj_identity'];

    // Extract form values.
    form_state_values_clean($form_state);

    foreach ($form_state['values'] as $key => $value) {
      if ($key != 'extra') {
        $entity->$key = $value;
      }
    }

    // Invoke all specified builders for copying form values to entity
    // properties.
    // @see entity_form_submit_build_entity()
    if (isset($form['#entity_builders'])) {
      foreach ($form['#entity_builders'] as $function) {
        $function('bmj_identity', $entity, $form, $form_state);
      }
    }

    field_attach_submit('bmj_identity', $entity, $form['extra'], $form_state);

    return $entity;
  }

}
