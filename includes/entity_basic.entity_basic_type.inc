<?php

/**
 * @file
 * Classes used for entity basic type.
 */

/**
 * The class used for Entity Basic Type entity.
 */
class EntityBasicType extends Entity {

  /**
   * The unique id this type.
   *
   * @var string
   */
  public $id;

  /**
   * The machine name of this type.
   *
   * @var string
   */
  public $type;

  /**
   * The human-redable name of this type.
   *
   * @var string
   */
  public $name;

  /**
   * The description this type.
   *
   * @var string
   */
  public $description = '';

  /**
   * The module.
   *
   * @var string
   */
  public $module = 'entity_basic';

  public function __construct($values = array()) {
    parent::__construct($values, 'entity_basic_type');
  }

}

/**
 * The Controller for the Basic Entity Type (CRUD).
 */
class EntityBasicTypeAPIControllerExportable extends EntityAPIControllerExportable {

  /**
   * Implements EntityAPIControllerInterface.
   */
  public function create(array $values = array()) {
    $values += array(
        'id' => '',
        'is_new' => TRUE,
        'description' => '',
    );
    return parent::create($values);
  }

  /**
   * Implements EntityAPIControllerInterface.
   *
   * @param $content
   *   Optionally. Allows pre-populating the built content to ease overridding
   *   this method.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $build = parent::buildContent($entity, $view_mode, $langcode, $content);
    return $build;
  }  
}

/**
 * UI controller for the Basic Entity Type.
 */
class EntityBasicTypeUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage basic types, including including fields, default status, etc.';
    return $items;
  }
}
