<?php

/**
 * @file
 * Contains pages for creating, editing, and deleting Entity Mail/Entity Mail Type.
 */

/**
 * List node administration filters that can be applied.
 *
 * @return
 *   An associative array of filters.
 */
function entity_basic_filters() {
  // Regular filters
  $filters['status'] = array(
    'title' => t('status'),
    'options' => array(
      '[any]' => t('any'),
      'status-1' => t('published'),
      'status-0' => t('not published'),
    ),
  );
  $filters['type'] = array(
    'title' => t('type'),
    'options' => array(
      '[any]' => t('any'),
    ) + node_type_get_names(),
  );
  return $filters;
}

/**
 * Applies filters for node administration filters based on session.
 *
 * @param $query
 *   A SelectQuery to which the filters should be applied.
 */
function entity_basic_build_filter_query(SelectQueryInterface $query) {
  // Build query
  $filter_data = isset($_SESSION['node_overview_filter']) ? $_SESSION['node_overview_filter'] : array();
  foreach ($filter_data as $index => $filter) {
    list($key, $value) = $filter;
    switch ($key) {
      case 'status':
        // Note: no exploitable hole as $key/$value have already been checked when submitted
        list($key, $value) = explode('-', $value, 2);
      case 'type':
      case 'language':
        $query->condition('n.' . $key, $value);
        break;
    }
  }
}

/**
 * Page callback: Form constructor for the content administration form.
 */
function entity_basic_admin_content($form, $form_state) {
  if (isset($form_state['values']['operation']) && $form_state['values']['operation'] == 'delete') {
    //return entity_basic_multiple_delete_confirm($form, $form_state, array_filter($form_state['values']['entity_basics']));
  }
  //$form['filter'] = entity_basic_filter_form();
  $form['#submit'][] = 'entity_basic_filter_form_submit';
  $form['admin'] = entity_basic_admin_list();

  return $form;
}

/**
 * Returns the node administration filters form array to node_admin_content().
 */
function entity_basic_filter_form() {
  $session = isset($_SESSION['entity_basic_overview_filter']) ? $_SESSION['entity_basic_overview_filter'] : array();
  $filters = entity_basic_filters();

  $i = 0;
  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Show only items where'),
    '#theme' => 'exposed_filters__node',
  );
  foreach ($session as $filter) {
    list($type, $value) = $filter;
    if ($type == 'term') {
      // Load term name from DB rather than search and parse options array.
      $value = module_invoke('taxonomy', 'term_load', $value);
      $value = $value->name;
    }
    elseif ($type == 'language') {
      $value = $value == LANGUAGE_NONE ? t('Language neutral') : module_invoke('locale', 'language_name', $value);
    }
    else {
      $value = $filters[$type]['options'][$value];
    }
    $t_args = array('%property' => $filters[$type]['title'], '%value' => $value);
    if ($i++) {
      $form['filters']['current'][] = array('#markup' => t('and where %property is %value', $t_args));
    }
    else {
      $form['filters']['current'][] = array('#markup' => t('where %property is %value', $t_args));
    }
    if (in_array($type, array('type', 'language'))) {
      // Remove the option if it is already being filtered on.
      unset($filters[$type]);
    }
  }

  $form['filters']['status'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('clearfix')),
      '#prefix' => ($i ? '<div class="additional-filters">' . t('and where') . '</div>' : ''),
  );
  $form['filters']['status']['filters'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('filters')),
  );
  foreach ($filters as $key => $filter) {
    $form['filters']['status']['filters'][$key] = array(
        '#type' => 'select',
        '#options' => $filter['options'],
        '#title' => $filter['title'],
        '#default_value' => '[any]',
    );
  }

  $form['filters']['status']['actions'] = array(
      '#type' => 'actions',
      '#attributes' => array('class' => array('container-inline')),
  );
  $form['filters']['status']['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => count($session) ? t('Refine') : t('Filter'),
  );
  if (count($session)) {
    $form['filters']['status']['actions']['undo'] = array('#type' => 'submit', '#value' => t('Undo'));
    $form['filters']['status']['actions']['reset'] = array('#type' => 'submit', '#value' => t('Reset'));
  }

  drupal_add_js('misc/form.js');

  return $form;
}

/**
 * Form builder: Builds the entity_basic administration overview.
 */
function entity_basic_admin_list() {
  // @todo check permission;
  $admin_access = TRUE;

  // Build the sortable table header.
  $header = array(
    'id' => array('data' => t('Id'), 'field' => 'e.id'),
    'type' => array('data' => t('Type'), 'field' => 'e.type'),
    'status' => array('data' => t('Status'), 'field' => 'e.status'),
    'changed' => array('data' => t('Updated'), 'field' => 'e.changed', 'sort' => 'desc')
  );

  $query = db_select('entity_basic', 'e')->extend('PagerDefault')->extend('TableSort');

  $ids = $query
  ->fields('e',array('id'))
  ->limit(50)
  ->orderByHeader($header)
  ->addTag('entity_basic_access')
  ->execute()
  ->fetchCol();
  $entity_basics = entity_basic_load_multiple($ids);

  // Prepare the list of entity_basic.
  $destination = drupal_get_destination();
  $options = array();
  foreach ($entity_basics as $entity_basic) {
    // Build a list of all the accessible operations for the current entity_basic.
    $operations = array();
    if (entity_basic_access('update', $entity_basic)) {
      $operations['edit'] = array(
        'title' => t('edit'),
        'href' => 'entity_basic/' . $entity_basic->id . '/edit',
        'query' => $destination,
      );
    }
    if (entity_basic_access('delete', $entity_basic)) {
      $operations['delete'] = array(
        'title' => t('delete'),
        'href' => 'entity_basic/' . $entity_basic->id . '/delete',
        'query' => $destination,
      );
    }
    $options[$entity_basic->id]['operations'] = array();
    if (count($operations) > 1) {
      // Render an unordered list of operations links.
      $options[$entity_basic->id]['operations'] = array(
        'data' => array(
          '#theme' => 'links__node_operations',
          '#links' => $operations,
          '#attributes' => array('class' => array('links', 'inline')),
        ),
      );
    }
    elseif (!empty($operations)) {
      // Render the first and only operation as a link.
      $link = reset($operations);
      $options[$entity_basic->id]['operations'] = array(
        'data' => array(
          '#type' => 'link',
          '#title' => $link['title'],
          '#href' => $link['href'],
          '#options' => array('query' => $link['query']),
        ),
      );
    }
  }

  // Only use a tableselect when the current user is able to perform any
  // operations.
  if ($admin_access) {
    $form['nodes'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No basic available.'),
    );
  }
  // Otherwise, use a simple table.
  else {
    $form['nodes'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $options,
      '#empty' => t('No basic available.'),
    );
  }

  $form['pager'] = array('#markup' => theme('pager'));
  return $form;
}

/**
 * Generate the Entity Basic Type editing form.
 */
function entity_basic_type_form($form, &$form_state, $entity_basic_type, $op = 'edit') {
  $field_language = NULL;

  if ($op == 'clone') {
    $entity_basic_type->type  = '';
    $entity_basic_type->name .= ' (cloned)';
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#default_value' => $entity_basic_type->name,
    '#description' => t('The human-readable name of this basic entity type.'),
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['type'] = array(
    '#type' => 'machine_name',
    '#title' => t('Name'),
    '#default_value' => $entity_basic_type->type,
    '#description' => t('A unique machine-readable name for this basic entity type. It must only contain lowercase letters, numbers, and underscores.'),
    '#maxlength' => 64,
    '#machine_name' => array(
      'exists' => 'entity_basic_type_load',
      'source' => array('name'),
    ),
    '#required' => TRUE,
    '#disabled' => FALSE,
  );
  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#default_value' => $entity_basic_type->description,
    '#description' => t('Describe this basic entity type. The text will be displayed on the <em>Add new basic</em> page.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save template'),
  );

  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => 'admin/structure/entity_basic_type',
  );

  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function entity_basic_form_submit(&$form, &$form_state) {
  $entity_basic_type = entity_ui_form_submit_build_entity($form, $form_state);
  $entity_basic_type->save();
  $form_state['redirect'] = 'admin/structure/entity_basic_type';
}

function entity_basic_add_page() {
  $controller = entity_ui_controller('entity_basic');
  return $controller->addPage();
}
