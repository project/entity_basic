<?php

/**
 * @file
 * Contains pages for creating, editing, and deleting Entity Mail/Entity Mail Type.
 */

/**
 * Generate the Entity Basic Type editing form.
 */
function entity_basic_type_form($form, &$form_state, $entity_basic_type, $op = 'edit') {
  $field_language = NULL;

  if ($op == 'clone') {
    $entity_basic_type->type  = '';
    $entity_basic_type->name .= ' (cloned)';
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#default_value' => $entity_basic_type->name,
    '#description' => t('The human-readable name of this basic entity type.'),
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['type'] = array(
    '#type' => 'machine_name',
    '#title' => t('Name'),
    '#default_value' => $entity_basic_type->type,
    '#description' => t('A unique machine-readable name for this basic entity type. It must only contain lowercase letters, numbers, and underscores.'),
    '#maxlength' => 64,
    '#machine_name' => array(
      'exists' => 'entity_basic_type_load',
      'source' => array('name'),
    ),
    '#required' => TRUE,
    '#disabled' => FALSE,
  );
  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#default_value' => $entity_basic_type->description,
    '#description' => t('Describe this basic entity type. The text will be displayed on the <em>Add new basic</em> page.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save template'),
  );

  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => 'admin/structure/entity_basic_type',
  );

  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function entity_basic_type_form_submit(&$form, &$form_state) {
  $entity_basic_type = entity_ui_form_submit_build_entity($form, $form_state);
  $entity_basic_type->save();
  $form_state['redirect'] = 'admin/structure/entity_basic_type';
}

