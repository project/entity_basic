<?php

/**
 * @file
 * Provides support for the Views module.
 */

/**
 * "EntityEmail" entity Views definition.
 */
class EntityEmailViewsController extends EntityDefaultViewsController {

  /**
   * Override views_data().
   */
  public function views_data() {
    $data = parent::views_data();
    return $data;
  }
}
